module type Droppable = sig
  type t
  val drop : t -> unit
end

(* In our example we simulate manual deallocation *)
let free x = ()

(* In our example we simulate testing for moving, as would be needed
   for instance when dropping what remains of a value after
   pattern-matching. *)
let moved x = false

(* The following example illustrates:
   - Tree-shaped data types
   - Mutually-recursive types
 *)

type ('a, 'b) atree =
  | ALeaf
  | ATree of ('a, 'b) btree * 'a * ('a, 'b) btree
and  ('a, 'b) btree =
  | BLeaf
  | BTreeA of ('a, 'b) atree * 'b
  | BTreeB of ('a, 'b) btree * 'b

module ABTree (A : Droppable) (B : Droppable) : Droppable with type t = (A.t, B.t) atree
  = struct
  type at = (A.t, B.t) atree
  type bt = (A.t, B.t) btree
  type t = at

  (* The definition of drop taken as a naive implementation, where in
     addition we show that we can handle two additional features of a
     realistic implementation:
      - we test for moving, as if it could have been moved,
      - we manually deallocate the cell, as if it was statically allocated.
     *)
  let rec drop_naive x =
    if not (moved x) then
      match x with
      | ALeaf -> ()
      | ATree (bt1, a, bt2) as cell -> (
          (* free bt1 *)
          drop_naive_b bt1 ;
          A.drop a ;
          (* free bt2 *)
          drop_naive_b bt2 ;
          free cell
        )
  and drop_naive_b x =
    if not (moved x) then
      match x with
      | BLeaf -> ()
      | BTreeA (at, b) as cell -> (
          (* free at *)
          drop_naive at ;
          B.drop b ;
          free cell
        )
      | BTreeB (bt, b) as cell -> (
          (* free bt *)
          drop_naive_b bt ;
          B.drop b ;
          free cell
        )

  (* drop_naive converted into CPS *)
  let rec drop_naive_cps x k =
    if moved x then
      k ()
    else
      match x with
      | ALeaf -> k ()
      | ATree (bt1, a, bt2) as cell -> (
          drop_naive_b_cps bt1 @@ fun () ->
          A.drop a ;
          drop_naive_b_cps bt2 @@ fun () ->
          free cell ;
          k ()
        )
  and drop_naive_b_cps x k =
    if moved x then
      k ()
    else
      match x with
      | BLeaf -> k ()
      | BTreeA (at, b) as cell -> (
          drop_naive_cps at @@ fun () ->
          B.drop b ;
          free cell ;
          k ()
        )
      | BTreeB (bt, b) as cell -> (
          drop_naive_b_cps bt @@ fun () ->
          B.drop b ;
          free cell ;
          k ()
        )

  (* defunctionalisation *)
  type drop_cont =
    | Top
    | KA1 of at * drop_cont * A.t * bt
    | KA2 of at * drop_cont
    | KB1 of bt * drop_cont * B.t
    | KB2 of bt * drop_cont * B.t

  let rec drop_naive_defunc x k =
    if moved x then
      return k
    else
      match x with
      | ALeaf -> return k
      | ATree (bt1, a, bt2) as cell -> drop_naive_b_defunc bt1 (KA1 (cell, k, a, bt2))
  and return = function
    | Top -> ()
    | KA1 (cell, k, a, bt2) -> A.drop a ; drop_naive_b_defunc bt2 (KA2 (cell, k))
    | KA2 (cell, k) -> free cell ; return k
    | KB1 (cell, k, b) -> B.drop b ; free cell ; return k
    | KB2 (cell, k, b) -> B.drop b ; free cell ; return k
  and drop_naive_b_defunc x k =
    if moved x then
      return k
    else
      match x with
      | BLeaf -> return k
      | BTreeA (at, b) as cell -> drop_naive_defunc at (KB1 (cell, k, b))
      | BTreeB (bt, b) as cell -> drop_naive_b_defunc bt (KB2 (cell, k, b))

  (* now we are going to re-use the cell:
     ATree becomes KA1 becomes KA2 becomes freed
     BTreeA becomes KB1 becomes freed
     BTreeB becomes KB2 becomes freed
  *)

  type drop_cont_reused =
    | Top
    | KA1 of { k : drop_cont_reused ; a : A.t ; bt2 : bt }
    | KA2 of { k : drop_cont_reused ; void : unit ; void2 : unit }
    | KB1 of { k : drop_cont_reused ; b : B.t }
    | KB2 of { k : drop_cont_reused ; b : B.t }

  type _ tag =
    | TagKA1 : at tag
    | TagKA2 : drop_cont_reused tag
    | TagKB1 : bt tag
    | TagKB2 : bt tag

  (* purple magic! re-use a cell *)
  let as_k : type a. a tag -> a -> drop_cont_reused -> drop_cont_reused = fun i x k ->
    let x = Obj.repr x |> Sys.opaque_identity in
    Obj.set_field x 0 (Obj.repr k) ;
    Obj.set_tag x (Obj.magic i) ;
    Obj.obj x

  let as_ka1 ~(cell:at) : drop_cont_reused -> drop_cont_reused =
    as_k TagKA1 cell
  let as_ka2 ~(cell:drop_cont_reused) : drop_cont_reused -> drop_cont_reused =
    as_k TagKA2 cell
  let as_kb1 ~(cell:bt) : drop_cont_reused -> drop_cont_reused =
    as_k TagKB1 cell
  let as_kb2 ~(cell:bt) : drop_cont_reused -> drop_cont_reused =
    as_k TagKB2 cell

  let rec drop_reuse_defunc x k =
    if moved x then
      return k
    else
      match x with
      | ALeaf -> return k
      | ATree (bt1, a, bt2) as cell -> drop_reuse_b_defunc bt1 (as_ka1 ~cell k)
  and return = function
    | Top -> ()
    | KA1 { k ; a ; bt2 } as cell -> A.drop a ; drop_reuse_b_defunc bt2 (as_ka2 ~cell k)
    | KA2 { k ; _ } as cell -> free cell ; return k
    | KB1 { k ; b ; _ } as cell -> B.drop b ; free cell ; return k
    | KB2 { k ; b ; _ } as cell -> B.drop b ; free cell ; return k
  and drop_reuse_b_defunc x k =
    if moved x then
      return k
    else
      match x with
      | BLeaf -> return k
      | BTreeA (at, b) as cell -> drop_reuse_defunc at (as_kb1 ~cell k)
      | BTreeB (bt, b) as cell -> drop_reuse_b_defunc bt (as_kb2 ~cell k)

  let drop x = drop_reuse_defunc x Top
end

(* Test *)

module Int : Droppable with type t = int
  = struct
  type t = int
  let drop int = Printf.printf "Dropping: %u\n" int
end

let example_value =
  let b3 = BTreeA (ALeaf, 5) in
  let a2 = ATree (BLeaf, 6, BTreeB(BLeaf, 7)) in
  let a1 = ATree (b3, 4, BLeaf) in
  let b1 = BTreeA (a1, 1) in
  let b2 = BTreeB (BTreeA(a2, 3), 2) in
  ATree (b1, 0, b2)

module M = ABTree (Int) (Int)

let _ = M.drop example_value
