// Demo of efficient drops
// rustc -O abtree.rs && ./abtree

use std::mem;
use std::ptr;


// set to 100000 to overflow the stack with
// the stock destructor
static N: i64 = 100000;


#[derive(Debug)]
pub enum ATree<A,B>
where A: std::fmt::Debug, B: std::fmt::Debug {
    ALeaf,
    ALeaf2, // prevent "Option" optim
    ATree(Box<BTree<A,B>>, A, Box<BTree<A,B>>),
}

#[derive(Debug)]
pub enum BTree<A,B>
where A: std::fmt::Debug, B: std::fmt::Debug {
    BLeaf,
    BTreeA(Box<ATree<A,B>>, A),
    BTreeB(Box<BTree<A,B>>, B),
}

#[derive(Debug)]
enum State {
    DropA,
    DropB,
    Return
}

#[allow(dead_code)]
#[derive(Debug)]
enum DropCont<A,B>
where A: std::fmt::Debug, B: std::fmt::Debug {
    Top,
    KA1(*mut DropCont<A,B>, A, Box<BTree<A,B>>),
    KA2(*mut DropCont<A,B>),
    KB(*mut DropCont<A,B>, B),
}

#[allow(dead_code)]
#[derive(Debug)]
enum Tag {
    Top,
    KA1,
    KA2,
    KB,
}

unsafe fn as_k<A,B,T>(tag: Tag, cell: *mut T, k: *mut DropCont<A,B>)
                      -> *mut DropCont<A,B>
where A: std::fmt::Debug, B: std::fmt::Debug {
    let cell: *mut [*mut (); 2] = mem::transmute(cell);
    (*cell)[0] = mem::transmute(tag as u64);
    (*cell)[1] = mem::transmute(k);
    mem::transmute(cell)
}

unsafe fn take_cell<T>(t: &mut Box<T>) -> *mut () {
    mem::transmute(Box::into_raw(ptr::read(t)))
}

unsafe fn reverse<A,B,T>(x: &mut *mut (),
                         tag: Tag,
                         new_x: &mut Box<T>,
                         k: &mut *mut DropCont<A,B>)
where A: std::fmt::Debug, B: std::fmt::Debug {
    let cell = mem::replace(x, take_cell(new_x));
    *k = as_k(tag, cell, *k);
}

unsafe fn pop_k<A,B>(k: &mut *mut DropCont<A,B>, next_k: &mut *mut DropCont<A,B>)
                     -> *mut DropCont<A,B>
where A: std::fmt::Debug, B: std::fmt::Debug {
    ptr::replace(k, ptr::read(next_k))
}

unsafe fn free_cell_as<A,B,T>(val: T, cell: *mut DropCont<A,B>)
where A: std::fmt::Debug, B: std::fmt::Debug {
    let a: *mut T = mem::transmute(cell);
    ptr::write(a, val);
    Box::from_raw(a);
}

fn drop<A,B>(x: ATree<A,B>)
where A: std::fmt::Debug, B: std::fmt::Debug {
    // hypotheses on the memory layout
    assert_eq!(8, mem::size_of::<A>());
    assert_eq!(8, mem::size_of::<B>());
    let mut top = DropCont::Top;
    let mut k = &mut top as *mut DropCont<A,B>;
    let mut state = State::DropA;
    unsafe {
        //always allocated with Box
        let mut x: *mut () = mem::transmute(Box::into_raw(Box::new(x)));
        unsafe fn read_as<T>(x: *mut ()) -> *mut T { mem::transmute(x) };
        loop {
            match state {
                State::DropA => match &mut *read_as::<ATree<A,B>>(x) {
                    ATree::ALeaf | ATree::ALeaf2 =>
                        state = State::Return,
                    ATree::ATree(bt1, _, _) => {
                        state = State::DropB;
                        reverse(&mut x, Tag::KA1, bt1, &mut k);
                    },
                },
                State::DropB => match &mut *read_as::<BTree<A,B>>(x) {
                    BTree::BLeaf =>
                        state = State::Return,
                    BTree::BTreeA(at, _) => {
                        state = State::DropA;
                        reverse(&mut x, Tag::KB, at, &mut k);
                    },
                    BTree::BTreeB(bt, _) => {
                        reverse(&mut x, Tag::KB, bt, &mut k);
                    },
                },
                State::Return => match &mut *k {
                    DropCont::Top => return,
                    DropCont::KA1(k2, a, bt2) => {
                        ptr::drop_in_place(a);
                        state = State::DropB;
                        x = take_cell(bt2);
                        k = as_k(Tag::KA2, k, ptr::read(k2));
                    },
                    DropCont::KA2(k2) => {
                        free_cell_as(ATree::ALeaf::<A,B>, pop_k(&mut k, k2));
                    },
                    DropCont::KB(k2, b) => {
                        ptr::drop_in_place(b);
                        free_cell_as(BTree::BLeaf::<A,B>, pop_k(&mut k, k2));
                    },
                },
            }
        }
    }
}

// Test

#[derive(Debug)]
struct Int { val: i64, }

impl Drop for Int {
    fn drop(&mut self) {
        //println!("Dropping {}", self.val)
    }
}

fn int(i: i64) -> Int {
    Int{val: i}
}

fn example_value(a: ATree<Int,Int>) -> ATree<Int,Int> {
    let b3 = BTree::BTreeA(Box::new(a), int(5));
    let a2 = ATree::ATree(Box::new(BTree::BLeaf), int(6),
                          Box::new(BTree::BTreeB(Box::new(BTree::BLeaf), int(7))));
    let a1 = ATree::ATree(Box::new(b3), int(4), Box::new(BTree::BLeaf));
    let b1 = BTree::BTreeA(Box::new(a1), int(1));
    let b2 = BTree::BTreeB(Box::new(BTree::BTreeA(Box::new(a2), int(3))), int(2));
    ATree::ATree(Box::new(b1), int(0), Box::new(b2))
}

fn main() {
    fn bench(drop_fun: fn(ATree<Int,Int>) -> (), print: bool) {
        let mut x: ATree<Int,Int> = ATree::ALeaf;
        for _i in 0..N {
            x = example_value(x);
        }
        let now = std::time::SystemTime::now();
        drop_fun(x);
        if print {
            println!("{:?}", now.elapsed().expect("Time went backwards"));
        }
    };
    bench(drop, false);
    println!("Custom drop with re-use");
    bench(drop, true);
    println!("Rust's compiler-generated drop");
    bench(mem::drop, true);
}


// Epilogue:

// This can be adapted to other smart pointers, provided they provide
// try_take_cell and free_cell functions that decompose their drop as
// follows. The resulting algorithm is non-allocating as long as the
// two functions are non-allocating.

#[allow(dead_code)]
struct Ptr<T> { a: std::marker::PhantomData<T> }

#[allow(dead_code)]
impl<T> Ptr<T> {
    fn try_take_cell(_a: Ptr<T>) -> Option<ptr::NonNull<T>> { None }

    unsafe fn free_cell(_a: ptr::NonNull<T>) { panic!("not mine!") }

    fn drop(a: Ptr<T>) {
        if let Some(ptr) = Ptr::try_take_cell(a) {
            unsafe {
                ptr::drop_in_place(ptr.as_ptr());
                Ptr::free_cell(ptr);
            }
        }
    }
}
